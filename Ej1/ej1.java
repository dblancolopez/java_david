import java.util.Scanner;


class Ej1{
    public static void main(String[] args) {
        Conversor conversor1 = new Conversor();
        Scanner lector = new Scanner(System.in);

        System.out.println("Indica que operación quieres hacer y el valor \n"
                           + "ejemplo: km/h a mi/h, 5"
                           + "(km/h, mi/h, m/s, yd/s)");
        System.out.println("Introduce primera medida: ");
        String medida1 = lector.next();
        System.out.println("Introduce medida final: ");
        String medida2 = lector.next();
        System.out.println("Introduce valor a convertir: ");
        double valor = lector.nextDouble();
        
        System.out.println(conversor1.operación(medida1, medida2, valor));
    }
}