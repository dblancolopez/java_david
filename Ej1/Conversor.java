class Conversor{
    public int km2m(int km){
        int respuesta = km*1000;
        return respuesta;
    }


    public double operacion( String desde, String hacia, double valor){
        double resultado = 0;

        if(desde.equals("m/s") && hacia.equals("km/h")){
            resultado = ms2km(valor);
        }else if(desde.equals("km/h") && hacia.equals("m/s")){
            resultado = kmh2ms(valor);
        }else if(desde.equals("mi/h") && hacia.equals("km/h")){
            resultado = mih2kmh(valor);
        }else if(desde.equals("km/h") && hacia.equals("mi/h")){
            resultado = kmh2mih(valor);
        }else if(desde.equals("km/h") && hacia.equals("yd/s")){
            resultado = km2yas(valor);
        }else if(desde.equals("yd/s") && hacia.equals("km/h")){
            resultado = yas2km(valor);
        }else if(desde.equals("mi/h") && hacia.equals("yd/s")){
            resultado = mih2yas(valor);
        }else if(desde.equals("yd/s") && hacia.equals("mi/h")){
            resultado = yas2mih(valor);
        }

        return resultado;
    }


    public double ms2km(double ms){
        double km;

        km = ms * 3.6;

        return km;
    }

    public double kmh2ms(double km){
        double ms;

        ms = km / 3.6;

        return ms;
    }

    public double mih2kmh(double mih){
        double resultado = 0;

        resultado = mih * 1.6093;

        return resultado;
    }

    public double kmh2mih(double kmh){
        double mih;

        mih = kmh / 1.6093;

        return mih;
    }

    public double km2yas(double km){
        double yas;

        yas = km / 3.292;

        return yas;
    }

    public double yas2km(double yas){
        double km;

        km = yas * 3.292;

        return km;
    }

    public double mih2yas(double mih){
        double yas;

        yas = mih / 2.045;

        return yas;
    }

    public double yas2mih(double yas){
        double mih;

        mih = yas * 2.045;

        return mih;
    }

}